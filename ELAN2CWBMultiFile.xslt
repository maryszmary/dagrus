<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions" xmlns:math="java.lang.Math">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no"/>
<xsl:strip-space elements="*"/>
<xsl:preserve-space elements=""/>

<xsl:param name="ELANDIR" select="'./ELAN-FILES/'"/>
<xsl:param name="SOUNDPARTS" select="'OUT/'"/>
<xsl:param name="SOUNDPARTSSERVER" select="'OUT/'"/>
<xsl:param name="FULLSOUNDS" select="'./ENDVERSION/'"/>
<xsl:param name="SOUNDEXT" select="'wav'"/>
<xsl:param name="allTiers" select="'no'"/>
<xsl:param name="excludeTiers" select="if($allTiers eq 'yes') then () else ('[Ii]nterviewer|[nN]otes|[Cc]omment|[Oo]ther')"/>



<xsl:template match="/">
<xsl:for-each select="collection($ELANDIR)">
<xsl:for-each select="ANNOTATION_DOCUMENT/TIER[if ($excludeTiers ne '') then ((not (matches(@TIER_ID, $excludeTiers)))) else (*)]/ANNOTATION/ALIGNABLE_ANNOTATION">
<xsl:sort select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID = current()/@TIME_SLOT_REF1]/@TIME_VALUE" data-type="number"/>
<!-- <xsl:message><xsl:value-of select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID = current()/@TIME_SLOT_REF1]/@TIME_VALUE" /></xsl:message>-->

<xsl:apply-templates select="."/>
</xsl:for-each>

<xsl:message>read eaf with sound file: <xsl:value-of select="./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL"/>; unique id: <xsl:value-of select="replace(./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/></xsl:message>

</xsl:for-each>



<xsl:result-document href="./SoundBatch.sh">
<!--<xsl:value-of select="concat('rm -rf ', $SOUNDPARTS,  '*', '&#10;')"/>-->
<xsl:apply-templates select="collection($ELANDIR)/ANNOTATION_DOCUMENT/TIER[if ($excludeTiers ne '') then ((not (matches(@TIER_ID, $excludeTiers)))) else (*)]/ANNOTATION/ALIGNABLE_ANNOTATION" mode="soundbatch"/>
</xsl:result-document>


<xsl:result-document href="./SoundBatch-server.sh">
<!--<xsl:value-of select="concat('rm -rf ', $SOUNDPARTS,  '*', '&#10;')"/>-->
<xsl:apply-templates select="collection($ELANDIR)/ANNOTATION_DOCUMENT/TIER[if ($excludeTiers ne '') then ((not (matches(@TIER_ID, $excludeTiers)))) else (*)]/ANNOTATION/ALIGNABLE_ANNOTATION" mode="soundbatchServer"/>
</xsl:result-document>

</xsl:template>


<xsl:template match="ANNOTATION_DOCUMENT/HEADER"/>
<xsl:template match="ANNOTATION_DOCUMENT">
<xsl:element name="doc">
<xsl:attribute name="file" select="replace(HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/>
<xsl:attribute name="date" select="replace(@DATE, '^(\d\d\d\d)-(\d\d)-(\d\d).*', '$1$2$3')"/>
<xsl:apply-templates/>
</xsl:element>

</xsl:template>
<xsl:template match="KONEC"/>

<xsl:template match="ALIGNABLE_ANNOTATION"/>

<xsl:template match="ALIGNABLE_ANNOTATION[ANNOTATION_VALUE[1] ne '']">
<xsl:element name="utterance">
<xsl:variable name="timeOrderFrom" select="@TIME_SLOT_REF1"/>
<xsl:variable name="timeOrderTo" select="@TIME_SLOT_REF2"/>
<xsl:attribute name="from" select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderFrom]/@TIME_VALUE"/>
<xsl:attribute name="to" select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderTo]/@TIME_VALUE"/>
<xsl:attribute name="file" select="replace(../../../HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/>
<xsl:attribute name="spkr" select="../../@TIER_ID"/>
<xsl:attribute name="fromId" select="@TIME_SLOT_REF1"/>
<xsl:attribute name="toId" select="@TIME_SLOT_REF2"/>
<xsl:attribute name="date" select="replace(../../../@DATE, '^(\d+)-(\d+)-(\d+).*', '$1$2$3')"/>
<xsl:apply-templates select="if (count(ANNOTATION_VALUE) &lt;2) then (ANNOTATION_VALUE) else (if (ANNOTATION_VALUE[@sts = 'ok']) then (ANNOTATION_VALUE[@sts = 'ok'][position() = last()]) else (ANNOTATION_VALUE[position() = last()]))"/>
</xsl:element>
<xsl:value-of select="'&#10;'"/>
</xsl:template>


<xsl:template match="ANNOTATION_VALUE">
<xsl:if test="@usr">
<xsl:attribute name="usr" select="if(@usr)then(@usr)else()"/>
<xsl:attribute name="sts" select="if(@sts)then(@sts)else()"/>
</xsl:if>
<xsl:value-of select="'&#10;'"/>
<xsl:analyze-string select="." regex="([\[\]\(\).:,!;?//\-&quot;&lt;&gt;=]|\s)">
<xsl:matching-substring>
<xsl:if test="matches(., '\S')">
<xsl:value-of select="."/>
<xsl:value-of select="'&#10;'"/>
</xsl:if>
</xsl:matching-substring>
<xsl:non-matching-substring>
<xsl:if test="matches(., '\S')">
<xsl:value-of select="."/>
<xsl:value-of select="'&#10;'"/>
</xsl:if>
</xsl:non-matching-substring>
</xsl:analyze-string>
</xsl:template>




<xsl:template match="ALIGNABLE_ANNOTATION[ANNOTATION_VALUE[1] ne '']" mode="soundbatch">
<xsl:variable name="file" select="replace(../../../HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/>
<xsl:variable name="timeOrderFrom" select="@TIME_SLOT_REF1"/>
<xsl:variable name="timeOrderTo" select="@TIME_SLOT_REF2"/>
<xsl:variable name="from" select="../../../TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderFrom]/@TIME_VALUE"/>
<xsl:variable name="to" select="../../../TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderTo]/@TIME_VALUE"/>
<xsl:value-of select="concat ('sox ', $FULLSOUNDS, $file,'.', $SOUNDEXT, ' ', $SOUNDPARTS,  $file,'-', $from, '-', $to,'.', $SOUNDEXT,' trim ' )"/>
<xsl:value-of select="floor (($from div 1000) div 3600)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 3600 div 60)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 60)"/>
<xsl:value-of select="'.'"/>
<xsl:value-of select="floor (($from mod 1000))"/>
<xsl:value-of select="' '"/>
<xsl:value-of select="floor (($to - $from) div 1000)"/>
<xsl:value-of select="'.'"/>
<xsl:value-of select="floor (($to - $from) mod 1000)"/>
<xsl:value-of select="'&#10;'"/>
</xsl:template>

<xsl:template match="ALIGNABLE_ANNOTATION[ANNOTATION_VALUE[1] ne '']" mode="soundbatchServer">
<xsl:variable name="file" select="replace(../../../HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/>
<xsl:variable name="timeOrderFrom" select="@TIME_SLOT_REF1"/>
<xsl:variable name="timeOrderTo" select="@TIME_SLOT_REF2"/>
<xsl:variable name="from" select="../../../TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderFrom]/@TIME_VALUE"/>
<xsl:variable name="to" select="../../../TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderTo]/@TIME_VALUE"/>
<xsl:value-of select="concat ('sox ', $FULLSOUNDS, $file,'.', $SOUNDEXT, ' ', $SOUNDPARTSSERVER,  $file,'-', $from, '-', $to,'.', $SOUNDEXT,' trim ' )"/>
<xsl:value-of select="floor (($from div 1000) div 3600)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 3600 div 60)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 60)"/>
<xsl:value-of select="'.'"/>
<xsl:value-of select="floor (($from mod 1000))"/>
<xsl:value-of select="' '"/>
<xsl:value-of select="floor (($to - $from) div 1000)"/>
<xsl:value-of select="'.'"/>
<xsl:value-of select="floor (($to - $from) mod 1000)"/>
<xsl:value-of select="'&#10;'"/>
</xsl:template>



</xsl:stylesheet>
