# usage: call the script as sudo ./REBUILDCORPUSwithSoundfiles.sh name, where name is name is the name you want to assosiate woith the corpus
# e.g., for rebuilding the dagrus corpus, write:
# ./REBUILDCORPUSwithSoundfiles.sh dagrus

NAME=$1
UPPER_NAME=$(echo $1 | tr '[:lower:]' '[:upper:]' )
echo name: $NAME.cwb.tag.comb
echo upper name: $UPPER_NAME"-UNTAGGED"

#tsee to that the correction facility can work because all files are writable
sh doRights.sh $NAME $UPPER_NAME
#backup ELAN files by zipping and copying into a simple directory
echo backup ELAN files
tar fzvc BACKUPS/ELAN-FILES-$(date +'%Y%m%d').tgz ELAN-FILES
echo backup corpus files
tar fzvc BACKUPS/$UPPER_NAME"-DATA"--$(date +'%Y%m%d').tgz ../$NAME/$UPPER_NAME"TMP"/Data

#we generate text files from ELAN files three times: (a) for a version of the corpus without interviewers (searchable!), (b) for a version of the corpus with interviewers (context view!), (c) for a running-text version of each ELAN files, so the interviews can be read as full text

#generate input for CWB from ELAN files, not interviewers
echo generate corpus files: only informants
java -Xms2500m -jar /opt/saxon9he.jar dummy.xml ELAN2CWBMultiFile.xslt allTiers="no" SOUNDEXT="wav" > $UPPER_NAME"-UNTAGGED"
# removing accents
cat $UPPER_NAME"-UNTAGGED" | sed -r "s/'//g" > $UPPER_NAME"-UNSTRESSED"

# #tag this twice with different options, merge them
echo tag twice
/opt/ttagger/bin/tree-tagger -token -lemma -sgml -no-unknown -cap-heuristics /opt/treetaggerlib/russian.par $UPPER_NAME"-UNSTRESSED" $NAME.cwb.tag.no-unknown
/opt/ttagger/bin/tree-tagger -token -lemma -sgml -cap-heuristics /opt/treetaggerlib/russian.par $UPPER_NAME"-UNSTRESSED" $NAME.cwb.tag.unknown
perl mergeMultipleTreeTagging.pl $NAME.cwb.tag.unknown $NAME.cwb.tag.no-unknown $NAME.cwb.tag.comb
python3 replace_unstressed.py $NAME.cwb.tag.comb $UPPER_NAME"-UNTAGGED"
  
# now, clean earlier versions, encode corpus to CWB 
mkdir -p $UPPER_NAME"TMP";
mkdir -p $UPPER_NAME"TMP"/Data;
mkdir -p $UPPER_NAME"TMP"/Registry;
mkdir -p $UPPER_NAME"TMP"/DataFull;
rm -rf $UPPER_NAME"TMP"/Data/* $UPPER_NAME"TMP"/Registry/* ;
echo encode corpus
cwb-encode -d $UPPER_NAME"TMP"/Data -f $NAME.cwb.tag.comb -R $UPPER_NAME"TMP"/Registry/$NAME -c utf8 -xsB -P tag -P lemma -P lemma2 -S utterance:0+id+file+from+to+spkr+fromId+toId+usr+sts+date 
cwb-makeall -r $UPPER_NAME"TMP"/Registry $UPPER_NAME

 
# ## #repeat the whole procedure for all tiers, including interviewers
echo generate corpus files: all tiers
java -Xms2500m -jar /opt/saxon9he.jar dummy.xml ELAN2CWBMultiFile.xslt allTiers="yes" SOUNDEXT="wav" > $UPPER_NAME"-UNTAGGED"
# removing accents
cat $UPPER_NAME"-UNTAGGED" | sed -r "s/'//g" > $UPPER_NAME"-UNSTRESSED"
echo tag
/opt/ttagger/bin/tree-tagger -token -lemma -sgml -no-unknown -cap-heuristics /opt/treetaggerlib/russian.par $UPPER_NAME"-UNSTRESSED" $NAME.cwb.tag.no-unknown
/opt/ttagger/bin/tree-tagger -token -lemma -sgml -cap-heuristics /opt/treetaggerlib/russian.par $UPPER_NAME"-UNSTRESSED" $NAME.cwb.tag.unknown
perl mergeMultipleTreeTagging.pl $NAME.cwb.tag.unknown $NAME.cwb.tag.no-unknown $NAME.cwb.tag.comb
python3 replace_unstressed.py $NAME.cwb.tag.comb $UPPER_NAME"-UNTAGGED"
echo encode corpus
cwb-encode -d $UPPER_NAME"TMP"/DataFull -f $NAME.cwb.tag.comb -R $UPPER_NAME"TMP"/Registry/$NAME"full" -c utf8 -xsB -P tag -P lemma -P lemma2 -S utterance:0+id+file+from+to+spkr+fromId+toId+usr+sts+date 
cwb-makeall -r $UPPER_NAME"TMP"/Registry $NAME"full"

#now convert third time, now for html full texts  
echo generate fulltexts
java -Xms2500m -jar /opt/saxon9he.jar dummy.xml ELAN2HTMLVersion.xslt allTiers="yes" SOUNDEXT="wav" -o:FullTexts.html
 
#call the utility that segments soundfiles  
echo recut all soundfiles 
sh SoundBatch-server.sh
echo finished!
sh doRights.sh $NAME $UPPER_NAME
