# DagRus

This project is aimed to make a corpus of Daghestanian Russian.
The project is based on the SpoCo platform. 

## Installing and configuring the platform

1. Clone this repository and create the necessary directories:

        > $ git clone https://maryszmary@bitbucket.org/maryszmary/dagrus.git

        > $ mkdir ELAN-FILES ENDVERSION

2. Put all your ELAN files to the ELAN-FILES directory and all wav files to ENDVERSION directory.

3. Install the following things:

    * CWB
    
    Use the development version of CWB here: http://cwb.sourceforge.net/.

    * treetagger
    
    Create the directories ttager and treetaggerlib in /opt and follow [this](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/) installation guide using ttager for installation and treetaggerlib as a storage for parameter files.
 
    * the HE version of saxon (so that there is a file saxon9he.jar in /opt)
    * sox

    For Ubuntu:

        > $ sudo apt-get install sox

4. Call the REBUILDCORPUSwithSoundfiles.sh script:

        > $ sudo ./REBUILDCORPUSwithSoundfiles.sh NAME

    where NAME is name is the name you want to assosiate woith the corpus.
    E.g., for building the DagRus corpus, I write:
    
        > $ sudo ./REBUILDCORPUSwithSoundfiles.sh dagrus
