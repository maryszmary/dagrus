﻿print "script to merge different annotations in vertical (treetagger) format\n";
print "input two annotated texts files, output first annotated file augmented with the last column of the second input file\n"; 
print "NOTE: a hack takes care of erroneously lemmatized <\n"; 


open (IN1, $ARGV[0]) or die; 
open (IN2, $ARGV[1]) or die; 
open (OUT, ">", $ARGV[2]) or die; 

for $inline1 (<IN1>){
	$inline2=<IN2>; 
	chomp($inline1); 
	chomp($inline2); 
	$outline=$inline1; 
	#add last annotation column if exists...
	#WE ALSO TAKE CARE OF &lt; which is erroneously lemmatized as UNKNOWN - this is a hack
	if ($inline2 =~/^&lt;/){
		$outline.='\t&lt;'
	} else{
	if ($inline2 =~/(\t\S+)$/){
		$outline.=$1;
	}}
	
	print OUT $outline."\n";
}	
