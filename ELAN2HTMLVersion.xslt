<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions" xmlns:math="java.lang.Math">
<xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
<xsl:strip-space elements="*"/>
<xsl:preserve-space elements=""/>

<xsl:param name="ELANDIR" select="'./ELAN-FILES'"/>
<xsl:param name="FULLSOUNDS" select="'./ENDVERSION/'"/>
<xsl:param name="SOUNDEXT" select="'wav'"/>


<xsl:param name="CompressedFiles" select="'../Compressed/'"/>
<xsl:param name="FullTextsDirectory" select="'./files_html/'"/>
 


<xsl:template match="/">
        <html>
            <head>
            	
            </head>
            <body>
<xsl:element name="h3">Свод текстов в корпусе</xsl:element>
<xsl:for-each select="collection($ELANDIR)">
	<xsl:sort data-type="number"/>
<xsl:variable name="fileName" select="concat($FullTextsDirectory, replace(./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1'), '.html')"/>
<xsl:element name="b"><xsl:value-of select="replace(./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/> - </xsl:element>
примерно <xsl:value-of select="count(//ALIGNABLE_ANNOTATION[ANNOTATION_VALUE[1] ne ''])"/> высказываний,   
<xsl:element name="a">
	<xsl:attribute name="href" select="$fileName"/>
аннотированный текст.  
	</xsl:element>
<br/>

<xsl:result-document href="{$fileName}">
        <html>
            <head>
            <script type="text/javascript">
		function play_sound(s) {
				document.getElementById(s).play();
		}
		</script>
            </head>
            <body>
<xsl:element name="h3"><xsl:value-of select="replace(./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/></xsl:element>
           <i><xsl:element name="a">
                <xsl:attribute name="target" select="'sound'"/>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($CompressedFiles, replace(./ANNOTATION_DOCUMENT/HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1'),  '.mp3')"/>
                        </xsl:attribute><xsl:value-of select="'Слушать в одном файле (mp3)'"/>
            </xsl:element>
          </i><br/>
<xsl:variable name="from" select="//TIME_ORDER/TIME_SLOT[1]/@TIME_VALUE"/>
<xsl:variable name="to" select="//TIME_ORDER/TIME_SLOT[position() eq last()]/@TIME_VALUE"/>

Первая транскрипция: 
<xsl:value-of select="floor (($from div 1000) div 3600)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 3600 div 60)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($from div 1000) mod 60)"/>

Последняя транскрипция: 
<xsl:value-of select="floor (($to div 1000) div 3600)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($to div 1000) mod 3600 div 60)"/>
<xsl:value-of select="':'"/>
<xsl:value-of select="floor (($to div 1000) mod 60)"/>

<br/><br/>

<xsl:variable name="OneFile">
<xsl:for-each select="ANNOTATION_DOCUMENT/TIER/ANNOTATION/ALIGNABLE_ANNOTATION">
<xsl:sort select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID = current()/@TIME_SLOT_REF1]/@TIME_VALUE" data-type="number"/>
<!-- <xsl:message><xsl:value-of select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID = current()/@TIME_SLOT_REF1]/@TIME_VALUE" /></xsl:message>-->
<xsl:apply-templates select="."/>
</xsl:for-each>
</xsl:variable>

<xsl:for-each-group select="$OneFile/utterance" group-adjacent="@spkr">
<br/><b><xsl:value-of select="concat(current-grouping-key(), ': ')"/></b>
<xsl:for-each select="current-group()">
<xsl:value-of select="."/>
            <xsl:element name="a">
                    <xsl:attribute name="target">_blank</xsl:attribute>
                    <xsl:attribute name="title">open in corpus interface</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('../context_xml_byTimeID.php?', '&amp;from=', @from, '&amp;file=', @file, '&amp;to=', @to)"/>
                        </xsl:attribute><xsl:value-of select="'ⁱ'"/>
            </xsl:element>
            <xsl:value-of select="' '"/>
            <xsl:element name="a">
                    <xsl:attribute name="title">listen</xsl:attribute>
                    <xsl:attribute name="href">
			<xsl:text>javascript:play_sound(&apos;</xsl:text>
                        <xsl:value-of select="concat(@file, '-',@from,'-', @to)"/>
			<xsl:text>&apos;);</xsl:text>
                        </xsl:attribute><xsl:value-of select="'♩'"/>
            </xsl:element>
            <xsl:value-of select="' '"/>
            <xsl:element name="audio">
                <xsl:attribute name="preload">none</xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:value-of select="concat(@file, '-',@from,'-', @to)"/>
                    </xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('../OUT/','')"/>
                        <xsl:value-of select="concat(@file, '-',@from,'-', @to, '.wav')"/>
                    </xsl:attribute>
		    <xsl:attribute name="type">audio/wav</xsl:attribute>
		    <xsl:element name="source">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('../OUT/','')"/>
                        <xsl:value-of select="concat(@file, '-',@from,'-', @to, '.wav')"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">audio/wav</xsl:attribute>
                </xsl:element>
            </xsl:element>           				 
                    

</xsl:for-each>
</xsl:for-each-group>
</body>
</html>
</xsl:result-document>

</xsl:for-each>

</body>
</html>



</xsl:template>


<xsl:template match="ANNOTATION_DOCUMENT/HEADER"/>
<xsl:template match="ANNOTATION_DOCUMENT"/>
<xsl:template match="KONEC"/>

<xsl:template match="ALIGNABLE_ANNOTATION"/>

<xsl:template match="ALIGNABLE_ANNOTATION[ANNOTATION_VALUE[1] ne '']">
<xsl:element name="utterance">
<xsl:variable name="timeOrderFrom" select="@TIME_SLOT_REF1"/>
<xsl:variable name="timeOrderTo" select="@TIME_SLOT_REF2"/>
<xsl:attribute name="from" select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderFrom]/@TIME_VALUE"/>
<xsl:attribute name="to" select="/ANNOTATION_DOCUMENT/TIME_ORDER/TIME_SLOT[@TIME_SLOT_ID eq $timeOrderTo]/@TIME_VALUE"/>
<xsl:attribute name="file" select="replace(../../../HEADER/MEDIA_DESCRIPTOR/@RELATIVE_MEDIA_URL, '.*?([^\\/]+)\.(mp3|wav|wma)', '$1')"/>
<xsl:attribute name="spkr" select="../../@TIER_ID"/>
<xsl:attribute name="fromId" select="@TIME_SLOT_REF1"/>
<xsl:attribute name="toId" select="@TIME_SLOT_REF2"/>
<xsl:value-of select="if (count(ANNOTATION_VALUE) &lt;2) then (ANNOTATION_VALUE) else (if (ANNOTATION_VALUE[@sts = 'ok']) then (ANNOTATION_VALUE[@sts = 'ok'][position() = last()]) else (ANNOTATION_VALUE[position() = last()]))"/>
</xsl:element>
<xsl:value-of select="'&#10;'"/>
</xsl:template>




</xsl:stylesheet>