import sys

with open(sys.argv[1]) as f:
    lines = f.read().split('\n')

with open(sys.argv[2]) as f:
    stressed = f.read().split('\n')

for i in range(len(lines)):
    if '\t' in lines[i] and lines[i][0] != '<':
        splited = lines[i].split('\t', 1)
 
        # the following is a hack for the situation if the lines are shifted for some reason
        if stressed[i].replace("'", "") == splited[0]:
            lines[i] = stressed[i] + '\t' + splited[1]
        elif stressed[i + 1].replace("'", "") == splited[0]:
            lines[i] = stressed[i + 1] + '\t' + splited[1]
        else:
            print("SOMETHING WHENT WRONG WHILE MERGING AT LINE " + str(I))


with open(sys.argv[1], 'w') as f:
    f.write('\n'.join(lines)) 
        
